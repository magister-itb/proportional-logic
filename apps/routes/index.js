"use strict"
const express = require('express')
const multer = require('multer')
const controller = require('../controllers')

module.exports = (app) => {
    const route = express.Router()
    app.use('/api', route)
    route.post('/validate', (req, res) => controller.default(req, res))
}