"use strict"

module.exports.default = async (req, res) => {
    let stopLoop = false
    try {
        let str = req.body.proportional.replace(/~/g, '!').replace(/∧/g, '&&').replace(/∨/g, '||').replace(/ /g, '')
        const calculate = async () => {
            if (str.includes('(')) {
                let tags = str.match(/\(([^()]*)\)/g)
                await tags.map(async (value, i) => {
                    let subValue = value
                    if (value.includes('true')) {
                        value = value.replace(/!true/g, '0')
                    }
                    if (value.includes('false')) {
                        value = value.replace(/!false/g, '1')
                    }

                    if (value.includes('!1')) {
                        value = value.replace(/!1/g, '0')
                    }
                    if (value.includes('!0')) {
                        value = value.replace(/!0/g, '1')
                    }

                    if (value.includes('->')) {
                        if (value.includes('0->1')) value = value.replace('0->1', '1')
                        if (value.includes('1->0')) value = value.replace('1->0', '0')
                        if (value.includes('0->0')) value = value.replace('0->0', '1')
                        if (value.includes('1->1')) value = value.replace('1->1', '1')
                    }
                    let logicResult = eval(value)
                    if (logicResult == 'true') logicResult = 1
                    if (logicResult == 'false') logicResult = 0
                    str = str.replace(subValue, logicResult)
                })
            } else {
                if (str.includes('true')) {
                    str = str.replace(/!true/g, '0')
                }
                if (str.includes('false')) {
                    str = str.replace(/!false/g, '1')
                }
                if (str.includes('!1')) {
                    str = str.replace(/!1/g, '0')
                }
                if (str.includes('!0')) {
                    str = str.replace(/!0/g, '1')
                }
                if (str.includes('->')) {
                    if (str.includes('0->1')) str = str.replace('0->1', '1')
                    if (str.includes('1->0')) str = str.replace('1->0', '0')
                    if (str.includes('0->0')) str = str.replace('0->0', '1')
                    if (str.includes('1->1')) str = str.replace('1->1', '1')
                }
                let logicResult = eval(str)
                str = str.replace(str, `${logicResult}`)
                if (str == 'true') str = 1
                if (str == 'false') str = 0
            }
            if (str == 0 || str == 1) {
                stopLoop = true
            }
        }

        while (stopLoop == false) { await calculate() }

        return res.status(200).json({
            proportional: req.body.proportional,
            result: str
        })
    } catch (error) {
        console.log(error)
        stopLoop = true
        return res.status(500).json({ message: 'Invalid Format' })
    }
}