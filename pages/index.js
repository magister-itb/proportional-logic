import React, { useEffect, useState } from 'react'
import axios from 'axios'

export default function Home() {

	function pad(str, max) {
		str = str.toString();
		return str.length < max ? pad("0" + str, max) : str;
	}

	const [error, setError] = useState('')
	const [equationLeft, setEquationLeft] = useState('')
	const [resultLeft, setResultLeft] = useState([])

	const [equationRight, setEquationRight] = useState('')
	const [resultRight, setResultRight] = useState([])

	const [equivalen, setEquivalen] = useState(null)
	const [header, setHeader] = useState([])

	// HANLDE INPUT LEFT
	const inputLeft = (e) => { setEquationLeft(e.target.value) }
	const leftConjunction = async () => { setEquationLeft(`${equationLeft} ∧ `) }
	const leftDisjuction = async () => { setEquationLeft(`${equationLeft} ∨ `) }
	// HANLDE INPUT RIGHT
	const inputRight = (e) => { setEquationRight(e.target.value) }
	const rightConjunction = async () => { setEquationRight(`${equationRight} ∧ `) }
	const rightDisjuction = async () => { setEquationRight(`${equationRight} ∨ `) }

	const [truthTable, setTruthTable] = useState([])

	const countVariables = async () => {
		let variable = `${equationLeft} <=> ${equationRight}`.match(/[a-z]/g)
		let uniqueVariable = [...new Set(variable)];
		return uniqueVariable.sort()
	}

	const calclulateProportionalLogic = async (data, type) => {
		try {
			let variable = await countVariables()
			let tableLogic = []
			for (let index = 0; index < Math.pow(2, variable.length); index++) {
				let binnary = pad(Number(index).toString(2), variable.length)
				tableLogic[index] = binnary.split('').map(value => (value == 0 ? '0' : '1'))
			}
			let result = []
			for (let index = 0; index < tableLogic.length; index++) {
				let equation = data
				await variable.map((val, i) => {
					equation = equation.replaceAll(`${val}`, `${tableLogic[index][i]}`)
				})
				equation = equation.replace(/~/g, '!').replace(/∧/g, '&&').replace(/∨/g, '||').replace(/ /g, '')
				const response = await axios.post('/api/validate', { proportional: equation })
				tableLogic[index][variable.length] = response.data.result
				result.push(+response.data.result)
			}
			if (type == 'left') {
				setResultLeft(result)
			}
			if (type == 'right') {
				setResultRight(result)
			}
		} catch (error) {
			setError('Invalid Format')
			return
		}

	}

	const calculate = async () => {
		setError('')
		if (!equationLeft || !equationRight) { return }
		let variable = await countVariables()
		setHeader(variable)
		let truth = []
		for (let index = 0; index < Math.pow(2, variable.length); index++) {
			let binnary = pad(Number(index).toString(2), variable.length)
			truth[index] = binnary.split('').map(value => (value == 0 ? '0' : '1'))
		}
		setTruthTable(truth)

		await calclulateProportionalLogic(equationLeft, 'left')
		await calclulateProportionalLogic(equationRight, 'right')

		if (JSON.stringify(resultLeft) == JSON.stringify(resultRight)) {
			setEquivalen(true)
		} else {
			setEquivalen(false)
		}
	}

	return (
		<div style={{ margin: '8px' }}>
			<div className="notification is-link">
				<p>Gunakan tanda <strong>"("</strong> dan <strong>")"</strong> untuk mengelompokan persamaan</p>
				<p>Gunakan tanda <strong>"~"</strong> untuk negasi</p>
				<p>Gunakan tombol <strong>"{'∧'}"</strong> untuk konjungsi</p>
				<p>Gunakan tombol <strong>"{'∨'}"</strong> untuk disjungsi</p>
				<p>Gunakan tanda <strong>"{'->'}"</strong> untuk implikasi</p>
			</div>
			<div style={{ display: 'flex', flexDirection: 'row' }}>
				<div>
					<input onChange={inputLeft} value={equationLeft} className="input is-rounded is-small" style={{ marginBottom: '4px' }}></input>
					<button onClick={leftConjunction} className="button is-small is-rounded is-info">∧</button>
					&nbsp;
					<button onClick={leftDisjuction} className="button is-small is-rounded is-info">∨</button>
				</div>
				&nbsp;
				<span style={{ fontWeight: 'bold' }}>{'<=>'}</span>
				&nbsp;
				<div>
					<input onChange={inputRight} value={equationRight} className="input is-rounded is-small" style={{ marginBottom: '4px' }}></input>
					<button onClick={rightConjunction} className="button is-small is-rounded is-info">∧</button>
					&nbsp;
					<button onClick={rightDisjuction} className="button is-small is-rounded is-info">∨</button>
				</div>
				&nbsp;
				<button onClick={calculate} className="button is-rounded is-success is-small">Check</button>
			</div>
			<br></br>
			{
				equivalen != null && error == '' ?
					<div className={`notification ${JSON.stringify(resultLeft) == JSON.stringify(resultRight) ? 'is-success' : 'is-warning'}`}>{
						JSON.stringify(resultLeft) == JSON.stringify(resultRight) ? 'Equivalen' : 'Tidak Equivalen'
					}</div> : error ? <div className={`notification is-danger`}>{error}</div> : ''

			}
			{/* <br></br> */}
			<div style={{ display: 'flex', flexDirection: 'row' }}>
				<div>
					{/* {
						equationLeft ? <div className="notification is-link" style={{ padding: '2px' }}>{equationLeft}</div> : ''
					} */}
					{
						error ? '' : <table className="table" style={{ margin: '0px' }}>
							<thead>
								<tr>
									{header.map(value => <th>{value}</th>)}
									<th>({equationLeft})</th>
									<th>({equationRight})</th>
									<th>Result</th>
								</tr>
							</thead>
							<tbody>
								{
									truthTable.map((val, i) => <tr style={{ padding: '2px', margin: '2px' }}>
										{
											val.map(data => <td style={{ padding: '2px', margin: '2px' }}>{`${data}`}</td>)
										}
										<td style={{ padding: '2px', margin: '2px' }} align="center">{resultLeft[i]}</td>
										<td style={{ padding: '2px', margin: '2px' }} align="center">{resultRight[i]}</td>
										<td style={{ padding: '2px', margin: '2px' }} align="center">{resultLeft[i] == resultRight[i] ? 1 : 0}</td>
									</tr>)
								}

							</tbody>
						</table>
					}

				</div>
				&nbsp;
				{/* <div>
					{
						equationRight ? <div className="notification is-link" style={{ padding: '2px' }}>{equationRight}</div> : ''
					}
					<table className="table" style={{ margin: '0px' }}>
						<thead>
							<tr>
								{
									rightheader.map(value => <th>{value}</th>)
								}
							</tr>
						</thead>
						<tbody>
							{
								rightResult.map((val, i) => <tr style={{ padding: '2px', margin: '2px' }}>
									{
										val.map(data => <td style={{ padding: '2px', margin: '2px' }}>{`${data}`}</td>)
									}
								</tr>)
							}
						</tbody>
					</table>
				</div> */}
			</div>
			<br></br>
		</div>
	)
}
