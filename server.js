const express = require('express')
const next = require('next')
require('dotenv').config()

const port = process.env.PORT || 3000
const dev = process.env.ENV !== 'production'
const app = next({ dev })
const handle = app.getRequestHandler()
const isDevelopment = process.env.ENV !== 'production'

app.prepare().then(() => {
  const bodyParser = require('body-parser')
  const cookieParser = require('cookie-parser')
  const server = express()
  // server.use(cors())
  server.use(bodyParser.urlencoded({ extended: true }))
  server.use(bodyParser.json())
  server.use(cookieParser())
  server.routes = require('./apps/routes')(server)
  server.all('*', (req, res) => {
    return handle(req, res)
  })

  server.listen(port, (err) => {
    if (err) throw err
    console.log(`> Ready on http://localhost:${port}`)
  })
}).catch(err => {
  console.log('Error:::::', err)
})